var _ = require('underscore');

const values = (testObj) => {
    return _.values(testObj);
}
module.exports = values;