var _ = require("underscore");

const mapObject = (testObj) => {
    let res = _.mapObject(testObj, function(value, key) {
        if(typeof value === 'string') {
            value += ' Appended';
        } else if (typeof value === 'number') {
            value -= 3;
        }
        return value
    });
    return res;
}

module.exports = mapObject;