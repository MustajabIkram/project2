var _ = require("underscore");

const invert = (testObj) => {
    return _.invert(testObj);
}

module.exports = invert;