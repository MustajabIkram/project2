var _ = require("underscore");

const defaults = (testObj) => {
    var defaultProps = { comicName: 'Batman' };

    if(typeof testObj !== 'object') return defaultProps;
    
    return _.defaults(testObj, defaultProps);
}
module.exports = defaults;