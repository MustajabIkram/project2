var _ = require('underscore');

const keys = (testObject) => {
    return _.keys(testObject);
}
module.exports = keys;